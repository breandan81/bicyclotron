PCBW = 50;
PCBH = 100;
SLAH = 101;
SLAW = 70;
SLAD= 47;
handleBarR = 22/2;
woodScrewR = 3.2/2;
contactClearance = 15;

//zipTieBox();
//scale(1.03)
//zipTieBoxLid();
//translate([-2,-2,30])
//scale(1.01)
dashBoxLid();
//dashBox();
//pcbFrame();
//pcbLock();
//headLight();
//headLightLid();
//gaugeHands();


module gaugeHands()
{
    linear_extrude(height = 3, center=false, convexity = 10)
    import("hand.dxf");
    }

module headLightLid()
{
       difference()
    {
    union()
    {

        linear_extrude(height = 3, center=false, convexity = 10)
    {
        import("headlightProfileoutside.dxf");
        }
    }
    //headlight
translate([40,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
translate([60,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
translate([50,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);

//left
translate([25,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
translate([15,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);

//right
translate([75,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
translate([85,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
    
translate([25,-1,10])
rotate([-90,0,0])
cylinder(r=3, h=10);
translate([50-19,-1,3])
cube([3,20,7]);
translate([50+19,-1,3])
cube([3,20,7]);
translate([50-19,-1,17])
cube([3,20,7]);
translate([50+19,-1,17])
cube([3,20,7]);  
//screw holes

translate([50,woodScrewR+1+9.5,0])
cylinder(r=woodScrewR, h = 35);

translate([woodScrewR+1,25.5,0])
cylinder(r=woodScrewR, h = 35); 

translate([100-(woodScrewR+1),25.5,0])
cylinder(r=woodScrewR, h = 35); 

translate([50,50-1-woodScrewR,0])
cylinder(r=woodScrewR, h = 35); 

linear_extrude(height = 1, center=false, convexity = 10)
    {
        import("headlightProfile.dxf");
        }
}
    }
module headLight()
{
   difference()
    {
    union()
    {
    linear_extrude(height = 30, center=false, convexity = 10)
    {
        import("headlightProfile.dxf");
        }
        linear_extrude(height = 3, center=false, convexity = 10)
    {
        import("headlightProfileoutside.dxf");
        }
    }
    //headlight
translate([40,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
translate([60,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
translate([50,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);

//left
translate([25,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
translate([15,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);

//right
translate([75,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
translate([85,30,15])
rotate([-90,0,0])
 cylinder(h = 30, r = 2.5);
    
translate([25,-1,10])
rotate([-90,0,0])
cylinder(r=3, h=10);
translate([50-19,-1,3])
cube([3,20,7]);
translate([50+19,-1,3])
cube([3,20,7]);
translate([50-19,-1,17])
cube([3,20,7]);
translate([50+19,-1,17])
cube([3,20,7]);  
//screw holes

translate([50,woodScrewR+1+9.5,0])
cylinder(r=woodScrewR, h = 35);

translate([woodScrewR+1,25.5,0])
cylinder(r=woodScrewR, h = 35); 

translate([100-(woodScrewR+1),25.5,0])
cylinder(r=woodScrewR, h = 35); 

translate([50,50-1-woodScrewR,0])
cylinder(r=woodScrewR, h = 35); 
}

    }
module pcbLock()
{
    difference()
    {
    cube([12,6,2]);
        translate([(12-7.9)/2, 3, 0])
    {
        cylinder(h=2, r = woodScrewR);
        translate([7.9,0,0])
                cylinder(h=2, r = woodScrewR);
        }
        
        }
    }
module pcbFrame()
{
    pcbL = 101.5;
    pcbW = 51;
    standoffH = 10;
    pcbT = 1.2;
    frameWidth = 6;
    supportWidth = 1;
    difference()
    {
        
    cube([pcbL+frameWidth*2, pcbW+frameWidth*2, pcbT+standoffH]);
    translate([supportWidth+frameWidth,supportWidth+frameWidth,0])
        cube([pcbL-2*supportWidth, pcbW-2*supportWidth, pcbT+standoffH]);
    translate([frameWidth + 2.54, frameWidth/2,0])    
    cylinder(r=woodScrewR, h = pcbT+standoffH);
    translate([frameWidth/2, frameWidth+2.54,0])    
    cylinder(r=woodScrewR, h = pcbT+standoffH);   
    
    translate([pcbL+frameWidth*2-(frameWidth + 2.54), frameWidth/2,0])        
    cylinder(r=woodScrewR, h = pcbT+standoffH);
    translate([pcbL+frameWidth*2-frameWidth/2, frameWidth+2.54,0])    
    cylinder(r=woodScrewR, h = pcbT+standoffH);   
        
    translate([pcbL+frameWidth*2-(frameWidth + 2.54), pcbW+frameWidth*2-frameWidth/2,0])        
    cylinder(r=woodScrewR, h = pcbT+standoffH);
    translate([pcbL+frameWidth*2-frameWidth/2, pcbW+frameWidth*2- (frameWidth+2.54),0])    
    cylinder(r=woodScrewR, h = pcbT+standoffH);   
        
     translate([(frameWidth + 2.54), pcbW+frameWidth*2-frameWidth/2,0])        
    cylinder(r=woodScrewR, h = pcbT+standoffH);
    translate([frameWidth/2, pcbW+frameWidth*2- (frameWidth+2.54),0])    
    cylinder(r=woodScrewR, h = pcbT+standoffH);   
    translate([frameWidth, frameWidth, standoffH])
    cube([pcbL, pcbW, pcbT]);
        }
    }

module screwMountBox()
{
batteryBox();
translate([20,(SLAW)/2,0])
rotate([0,0,90])
cruiserClamp();
translate([70,(SLAW)/2,0])
rotate([0,0,-90])
cruiserClamp();
}
module spacer()
{
height = 40;
uboltr= 8/2;
fudge = 1.15;
studr = 6;
difference()
{
translate([-35,-20,0])   
cube([70,40,height]);
    //cylinders for the ubolt
    translate([-26.25, -12.75, 0])
    cylinder(r=uboltr*fudge, h=height);
    translate([26.25, -12.75, 0])
    cylinder(r=uboltr*fudge, h=height);
    //cylinders for enginestuds
    
    translate([-21.5,10,0])
    cylinder(h=20, r=studr*fudge);
    translate([21.5,10,0])
    cylinder(h=20, r=studr*fudge);
    translate([0,50,height+22])
rotate([90+18,0,0])
cylinder(h=100, r=22);
}
}

//SLA stuff
module SLA()
{

    union()
    {
    cube([SLAH, SLAW, SLAD]);
    translate([-contactClearance,0,0])
    cube([contactClearance, SLAW , contactClearance]);
    translate([-contactClearance,00,SLAD-contactClearance])
    cube([contactClearance, SLAW , contactClearance]);
    }
}
module SLACube()
{
    translate([-contactClearance,0,0])
    cube([SLAH+contactClearance, SLAW, SLAD]);
    }
module batteryBox()
{
      thickness=10;
    translate([0,0,thickness])
    difference()
    {
  
    translate([-contactClearance-thickness,0,-thickness])
    cube([SLAH+thickness*2+contactClearance,SLAW+thickness, SLAD+thickness*2]);
    SLACube();
    translate([0, SLAW/2, thickness])
    cylinder(r=3, h = SLAD);
    translate([-contactClearance-thickness/2,0,-thickness/2])
    rotate([-90,0,0])
    cylinder(r=woodScrewR, h = 20);
    
    translate([-contactClearance-thickness/2,0,-thickness/2])
    rotate([-90,0,0])
    cylinder(r=woodScrewR, h = 20);
    
        
    translate([-contactClearance-thickness/2+((SLAH+contactClearance+thickness)/2),0,-thickness/2])
    rotate([-90,0,0])
    cylinder(r=woodScrewR, h = 20);
    
    translate([-contactClearance-thickness/2+((SLAH+contactClearance+thickness)),0,-thickness/2])
    rotate([-90,0,0])
    cylinder(r=woodScrewR, h = 20);
    translate([0,0,SLAD+thickness])
    {
    translate([-contactClearance-thickness/2,0,-thickness/2])
    rotate([-90,0,0])
    cylinder(r=woodScrewR, h = 20);
    
        
    translate([-contactClearance-thickness/2+((SLAH+contactClearance+thickness)/2),0,-thickness/2])
    rotate([-90,0,0])
    cylinder(r=woodScrewR, h = 20);
    
    translate([-contactClearance-thickness/2+((SLAH+contactClearance+thickness)),0,-thickness/2])
    rotate([-90,0,0])
    cylinder(r=woodScrewR, h = 20);
    }
}
}

module zipTieBox()
{
    rBar = 17/2;
    dBar = 37;
    thickness = 2;
    difference()
    {
    cube([SLAW+2*thickness, SLAD+2*thickness, SLAH+thickness*1+contactClearance]);
        translate([thickness,thickness,thickness])
        cube([SLAW, SLAD, SLAH+contactClearance]);
         translate([-10,SLAD*.5+thickness,SLAH+thickness*1+contactClearance-5])
rotate([0,90,0])
cylinder(r=woodScrewR, h = 100);  
    }
    translate([8,-rBar-thickness, 0])
    difference()
    {
    cube([rBar*2+thickness *2, rBar+thickness, SLAH+thickness]);
    translate([rBar+thickness,0,0])
    cylinder(h= SLAH+thickness, r = rBar);
    translate([0,rBar+thickness-2,10])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,20])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,30])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,40])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,50])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,60])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,70])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,80])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,9s0])
    cube([rBar*2+thickness *2+1,2,5]);
    
   
}
translate([dBar+8,-rBar-thickness, 0])
    difference()
    {
    cube([rBar*2+thickness *2, rBar+thickness, SLAH+thickness]);
    translate([rBar+thickness,0,0])
    cylinder(h= SLAH+thickness, r = rBar);
    translate([0,rBar+thickness-2,10])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,20])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,30])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,40])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,50])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,60])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,70])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,80])
    cube([rBar*2+thickness *2+1,2,5]);
     translate([0,rBar+thickness-2,9s0])
    cube([rBar*2+thickness *2+1,2,5]);
}
  
}
module zipTieBoxLid()
{
    
    thickness = 2;
    difference()
    {
    cube([SLAW+4*thickness, SLAD+4*thickness, SLAH+thickness*2+contactClearance]);
    cube([SLAW+4*thickness, SLAD+4*thickness, SLAH+thickness*1+contactClearance-15]);
    translate([thickness,thickness,0])
    cube([SLAW+2*thickness, SLAD+2*thickness, SLAH+thickness*1+contactClearance]);
    translate([-10,SLAD*.5+thickness,SLAH+contactClearance-5-1])
    rotate([0,90,0])
    cylinder(r=woodScrewR, h = 100);  
    translate([(SLAW+2*thickness)/2, (SLAD+2*thickness)/2, 0])
    cylinder(r=5, h= 500);    

        }
}
zipTieMount()
{
    
    }
module cruiserClamp()
{
    nutR = 13/2;
    rBar = 17/2;
    dBar = 37;
    extra = 38;
    width = 30;
    rScrew = 9/2*1.1;
//    xSize = -(dBar+rBar*2+extra);
    xSize = SLAW+20;
    translate([0,0,-(rBar + extra/2)])
    difference()
    {
    translate([-xSize/2,0,0])
    cube([xSize, width, rBar + extra/2]);
    translate([-dBar/2, 0,0])
    rotate([-90,0,0])
    cylinder(h=width, r = rBar);
     translate([dBar/2, 0,0])
    rotate([-90,0,0])
    cylinder(h=width, r = rBar);
    translate([0,15,0])
    holeWithTrap();
    translate([(dBar/2)+rBar+(extra/4),15,0])
    holeWithTrap();
    translate([-((dBar/2)+rBar+(extra/4)),15,0])
    holeWithTrap();
    }

    
}
module holeWithTrap()
{
    cylinder(r= 9/2*1.1, h = 25*.75);
    translate([-13.5/2, -13.5/2,10])
    cube([13.5,13.5+15,7]);
    }
module cruiserClampLock()
    {
         nutR = 13/2;
    rBar = 17/2;
    dBar = 37;
    extra = 38;
    width = 30;
    rScrew = 9/2*1.1;
    
    difference()
    {
    translate([-(dBar+rBar*2+extra)/2,0,0])
    cube([dBar+rBar*2+extra, width, rBar+6]);
    translate([-dBar/2, 0,-1])
    rotate([-90,0,0])
    cylinder(h=width, r = rBar);
     translate([dBar/2, 0,-1])
    rotate([-90,0,0])
    cylinder(h=width, r = rBar);
    translate([0,15,0])
     cylinder(r= 9/2*1.1, h = 25*.75);
    translate([(dBar/2)+rBar+(extra/4),15,0])
    cylinder(r= 9/2*1.1, h = 25*.75);
    translate([-((dBar/2)+rBar+(extra/4)),15,06])
   cylinder(r= 9/2*1.1, h = 25*.75);
    }

    
        
        }
        
//zip tie version of frame clamp for battery box
module circuitBox()
{
    
    
   }
module circuitClamp()
{
  }
module dummyGauge()
{
    cylinder(r=28, h = 37);
    }  
module dashBox()
{
    height = 75;
    width = 150;
    thick = 30;
    wall = 2;
    difference()
    {
    union(){
    translate([height/2-wall,-wall,-wall])
    cube([width-height+2*wall,height+2*wall,thick+wall]);
    translate([height/2,height/2, -wall])
    cylinder(r=height/2+wall, h=thick+wall); 
    translate([height/2+width-height,height/2, -wall])   
    cylinder(r=height/2+wall, h=thick+wall);  
    }
    translate([height/2,0,0])
    cube([width-height,height,thick]);
    translate([height/2,height/2, 0])
    cylinder(r=height/2, h=thick); 
    translate([height/2+width-height,height/2, 0])   
    cylinder(r=height/2, h=thick);     
    }
    translate([40,-30,0])
    mountTab();
     translate([width-40,-30,0])
    mountTab();
//        translate([31+3,38,2])
//            dummyGauge();
//         translate([150-31-3,38,2])
//            dummyGauge();
        }
 module mountTab()
{
    screwR = 3.5;
    difference()
    {
    cube([4,30,15]);
    translate([-2,7.5,7.5])
    rotate([0,90,0])
    cylinder(r=screwR, h=10);
    
    }
}
module dashBoxLid()
{
    height = 75;
    width = 150;
    thick = 10;
    wall = 2;
    wall2 = 4;
    difference()
    {
    translate([-wall,-wall,0])
    union(){
    translate([height/2,0,0])
    cube([width-height+2*wall2,height+2*wall2,thick+wall2]);
    translate([height/2+wall2,height/2+wall2, 0])
    cylinder(r=height/2+wall2, h=thick+wall); 
    translate([height/2+width-height+wall2,height/2+wall2, 0])   
    cylinder(r=height/2+wall2, h=thick+wall);  
              
         translate([33+wall2,75/2+wall2,2])
            cylinder(r=33, h = 25);
         translate([150-33+wall2,75/2+wall2,2])
           cylinder(r=33, h = 25);
    }
    
    union(){
    translate([height/2,0,0])
    cube([width-height+2*wall,height+2*wall,thick]);
    translate([height/2+wall,height/2+wall, 0])
    cylinder(r=height/2+wall, h=thick); 
    translate([height/2+width-height+wall,height/2+wall, 0])   
    cylinder(r=height/2+wall, h=thick);  
      translate([-wall,-wall,0])
       {
        translate([33+wall2,75/2+wall2,2])
            dummyGauge();
         translate([150-33+wall2,75/2+wall2,2])
           dummyGauge();
       }
    }
    translate([width/2+wall,15,0])
    cylinder(r=6, h= 30);
    translate([width/2,-10,5])
    rotate([-90,0,0])
    cylinder(r=woodScrewR,h=150);
    }
    
       
}