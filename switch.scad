switchHolder();
module switchHolder()
{
    swW=20;
    swT=6.3;
  
  union(){
    difference()
    {
    translate([-2,0,0])
    cube([swW+4, 5, swT+2]);
    translate([-.25,0,0])
    cube([swW+.5, 5, swT+.5]);
    translate([.5,0,0])
    cube([swW-1, 5, swT+2]);
        }
        translate([5,2.5,0])
   cylinder(h=2,r=.8);
         translate([15,2.5,0])
   cylinder(h=2,r=.8);
   translate([-2,0,-2])
        cube([swW+4,5,2]);
}
}